import xmlrpclib

# Inisiasi remote client (xml rpc proxy)
proxy = xmlrpclib.ServerProxy("http://localhost:6666/")

a = 10
b = 90

# Jalankan fungsi tambah di server
hasil = proxy.tambah(a,b)
print a,"+",b,"=", hasil
# Jalankan fungsi kurang di server
hasil = proxy.kurang(a,b)
print a,"-",b,"=", hasil

hasil = proxy.bagi(a,b)
print a,"/",b,"=", hasil

hasil = proxy.kali(a,b)
print a,"*",b,"=", hasil

hasil = proxy.banding(a,b)
print a,"banding",b,"=", hasil