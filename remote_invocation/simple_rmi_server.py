import Pyro4
from hitung import Penghitung

# Konfigurasi security key
Pyro4.config.HMAC_KEY="12345"
# Inisiasi object Pyro4 daemon, parameter : IP dan Port
server_daemon = Pyro4.Daemon(host="", port=9555)

# Inisiasi object Penghitung
penghitung = Penghitung()
penghitung_dua = Penghitung()
# Jalankan server, parameter : dictionary object, nameserver, daemon
Pyro4.Daemon.serveSimple(
		{
			penghitung: "penghitung",
			penghitung_dua : "penghitungdua"
		},
		ns = False,
		daemon = server_daemon)
