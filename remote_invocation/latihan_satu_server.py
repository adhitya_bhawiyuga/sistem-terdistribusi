import xmlrpclib
from SimpleXMLRPCServer import SimpleXMLRPCServer

# Buat fungsi
def tambah(a,b):
	return (a+b)

def kurang(a,b):
	return (a-b)

def bagi(a,b):
	return (a/b)

def kali(a,b):
	return (a*b)

def banding(a,b):
	if a > b :
		return str(a)+" lebih besar dari "+str(b)
	else :
		return str(b)+" lebih besar dari "+str(a)

# Insiasi object xml rpc server
server = SimpleXMLRPCServer( ("", 6666) )
print "Listening on port 6666..."
# Registrasi fungsi di server
server.register_function(tambah, "tambah")
server.register_function(kurang, "kurang")
server.register_function(kali, "kali")
server.register_function(bagi, "bagi")
server.register_function(banding, "banding")
# Jalankan server
server.serve_forever()