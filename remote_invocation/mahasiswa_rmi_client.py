import Pyro4
from mahasiswa import Mahasiswa

# Konfigurasi security key
Pyro4.config.HMAC_KEY="12345"
# Inisiasi remote object
pengecek = Pyro4.Proxy("PYRO:pengecek@127.0.0.1:9555")

# Inisiasi object mahasiswa
mhs = Mahasiswa("Adhitya", "12345", 2.9)
print "Sebelum : Nama", mhs.nama, "status", mhs.status
# Panggil method
mhs = pengecek.cek_mahasiswa(mhs)
print "Sesudah : Nama", mhs.nama, "status", mhs.status
