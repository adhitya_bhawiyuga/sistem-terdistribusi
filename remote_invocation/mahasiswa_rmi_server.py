import Pyro4
import mahasiswa

class Pengecek(object) :
	def cek_mahasiswa(self, mahasiswa) :		
		if mahasiswa.ipk < 3.0 :
			mahasiswa.status = "TERANCAM"
		else :
			mahasiswa.status = "AMAN"
		return mahasiswa

# Konfigurasi security key
Pyro4.config.HMAC_KEY="12345"
# Inisiasi object Pyro4 daemon, parameter : IP dan Port
daemon = Pyro4.Daemon(host="", port=9555)

# Inisiasi object
pengecek = Pengecek()

# Jalankan server, parameter : dictionary object, nameserver, daemon
Pyro4.Daemon.serveSimple(
		{
			pengecek: "pengecek"
		},
		ns = False,
		daemon = daemon)
