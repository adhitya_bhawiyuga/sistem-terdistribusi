#!flask/bin/python
from flask import Flask, request
import json

app = Flask(__name__)

list_pegawai = [
	{
		'id': 1,
		'nama': 'Paijo',
		'alamat': 'Malang'
	},
	{
		'id': 2,
		'nama': 'Paino',
		'alamat': 'Surabaya'
	}
]

@app.route('/api/v1.0/pegawai', methods=['GET'])
def get_all_pegawai():
	return json.dumps({'pegawai': list_pegawai})

@app.route('/api/v1.0/pegawai/<int:pegawai_id>', methods=['GET'])
def get_pegawai(pegawai_id):
	pegawai = [p for p in list_pegawai if p['id'] == pegawai_id]
	if len(pegawai) == 0:
		abort(404)
	return json.dumps({'pegawai': pegawai[0]})

@app.route('/api/v1.0/pegawai', methods=['POST'])
def create_pegawai():
	pegawai = {
		'id': list_pegawai[-1]['id'] + 1,
		'nama': request.form.get('nama'),
		'alamat': request.form.get('alamat', "")
	}
	list_pegawai.append(pegawai)
	return json.dumps({'pegawai': list_pegawai})
	#return json.dumps({'pegawai': pegawai}), 201

@app.route('/api/v1.0/pegawai/<int:pegawai_id>', methods=['DELETE'])
def delete_pegawai(pegawai_id):
	pegawai = [p for p in list_pegawai if p['id'] == pegawai_id]
	if len(pegawai) == 0:
		abort(404)
	list_pegawai.remove(pegawai[0])
	return json.dumps({'result': True})

@app.route('/api/v1.0/pegawai/<int:pegawai_id>', methods=['PUT'])
def update_pegawai(pegawai_id):
	pegawai = [p for p in list_pegawai if p['id'] == pegawai_id]
	if len(pegawai) == 0:
		abort(404)
	nama = request.form.get('nama')
	alamat = request.form.get('alamat', "")
	pegawai[0]["nama"] = nama
	pegawai[0]["alamat"] = alamat
	return json.dumps({'result': True})

if __name__ == '__main__':
	app.run(debug=True)