import socket, sys

# Server IP address
SERVER_IP = '127.0.0.1'
# Port number used by server
PORT = 6666
# Maximum size of UDP packet
MAX = 65535
# Initialize socket object with UDP/Datagram type
s = socket.socket(socket.AF_INET, socket.SOCK_DGRAM)

# CONNECTIONLESS, DOESN'T NEED TO BUILD CONNECTION
# You can directly send the message
s.sendto('This is my message', (SERVER_IP, PORT))
# Read message from server with the buffer size = maximum size of UDP
data, address = s.recvfrom(MAX)
print 'The server', address, 'says', repr(data)