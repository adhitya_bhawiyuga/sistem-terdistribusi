import socket, sys
import json

# Server IP address
SERVER_IP = '127.0.0.1'
# Port number used by server
PORT = 5555
# Initialize socket object with TCP/Stream type
s = socket.socket(socket.AF_INET, socket.SOCK_STREAM)

print 'Address before sending:', s.getsockname()
# Initiate a CONNECTION
s.connect((SERVER_IP, PORT))
# Build json data
mhs = {"nama" : "Adhitya", "nim" : "123456789"}
json_data = json.dumps(mhs)
print json_data
# Send the message
s.sendall(json_data)
s.close()

