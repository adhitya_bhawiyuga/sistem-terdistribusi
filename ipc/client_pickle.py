import socket, sys
import pickle

class Mahasiswa(object):
	nama = ""
	nim = ""

	def __init__(self, nama_mhs, nim_mhs):
		self.nama = nama_mhs
		self.nim = nim_mhs

# Server IP address
SERVER_IP = '127.0.0.1'
# Port number used by server
PORT = 5555
# Initialize socket object with TCP/Stream type
s = socket.socket(socket.AF_INET, socket.SOCK_STREAM)

print 'Address before sending:', s.getsockname()
# Initiate a CONNECTION
s.connect((SERVER_IP, PORT))

# Build json data
mhs = Mahasiswa("Adhitya", "123456789")
pickle_data = pickle.dumps(mhs)

print pickle_data
# Send the message
print s.sendall(pickle_data)
s.close()

