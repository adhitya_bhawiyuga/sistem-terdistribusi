import socket, sys

# Server IP address
SERVER_IP = '127.0.0.1'
# Port number used by server
PORT = 5555

# Initialize socket object with TCP/Stream type
s = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
# Initiate a CONNECTION
s.connect((SERVER_IP, PORT))
input = ''
while True :
	try :
		input = raw_input("Masukkan pesan yang akan dikirim : ")
		# Send the message
		s.sendall(input)
		# Read message stream from server 
		data, address = s.recvfrom(4096)
		print 'Server mengirimkan data', data
	except KeyboardInterrupt :
		break		
s.close()