import socket, sys

# Port number
PORT = 6666
# Maximum size of UDP packet
MAX = 65535 
# Initialize socket object with UDP/Datagram type
s = socket.socket(socket.AF_INET, socket.SOCK_DGRAM)

# Bind to a certain IP and PORT use '' to accept incoming packet from anywhere
s.bind(('', PORT))
# Iterate forever during program running to avoid program close
while True:
	# Read the message from socket with the buffer size = maximum size of UDP
	data, address = s.recvfrom(MAX)
	print 'The client at', address, 'says', repr(data)
	# Send back the message to client
	s.sendto('OK '+data, address)