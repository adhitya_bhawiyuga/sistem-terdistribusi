import socket
import pickle

class Mahasiswa(object) :
	nama = ""
	nim = ""

	def __init__(self,namaMhs, nimMhs) :
		self.nama = namaMhs
		self.nim = nimMhs

# Server IP address
SERVER_IP = '127.0.0.1'
# Port number
PORT = 6666
# Inisialisasi object socket
s = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
# connect to server
s.connect((SERVER_IP, PORT))
# Format data 
mhs = Mahasiswa("Adhitya", "123456789")
# Marshalling
n = pickle.dumps(mhs)
print n
# Kirim
s.sendall(n)
s.close()