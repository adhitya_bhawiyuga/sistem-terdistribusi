import socket

# Port number
PORT = 6666
# Inisialisasi socket TCP
s = socket.socket(socket.AF_INET, socket.SOCK_STREAM)

# Bind Port
s.bind(('', PORT))
# Listen
s.listen(10)

while True:
	# Accepting incoming connection
	conn, address = s.accept()
	# Baca pesan dari client
	data, address = conn.recvfrom(4096)
	print "The client at ", address, ' send ', data
	# Kirim balik ke client
	conn.sendall('OK '+data)	
	conn.close()