import socket

# Port number
PORT = 6666
# Maximum size 
MAX = 65535
# Inisialisasi socket UDP
s = socket.socket(socket.AF_INET, socket.SOCK_DGRAM)

# Bind Port
s.bind(('', PORT))

while True:
	# Baca pesan dari client
	data, address = s.recvfrom(MAX)
	print "The client at ", address, ' send ', data
	# Kirim balik ke client
	s.sendto('OK '+data, address)	