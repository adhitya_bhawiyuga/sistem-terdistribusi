import socket

# Server IP address
SERVER_IP = '127.0.0.1'
# Port number
PORT = 6666
# Maximum size
MAX = 65535
# Inisialisasi object socket
s = socket.socket(socket.AF_INET, socket.SOCK_DGRAM)
user_input = raw_input("Masukkan pesan yang akan dikirim : ")
# Kirim
s.sendto(user_input, (SERVER_IP, PORT))
# Baca kiriman balik dari Server
data, address = s.recvfrom(MAX)
print 'The server answer ', data