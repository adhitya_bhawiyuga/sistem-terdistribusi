import socket
import json

# Server IP address
SERVER_IP = '127.0.0.1'
# Port number
PORT = 6666
# Inisialisasi object socket
s = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
# connect to server
s.connect((SERVER_IP, PORT))
# Format data 
mahasiswa = {"nama" : "Adhitya", "nim" : "123456789"}
# Marshalling
n = json.dumps(mahasiswa)
print n
# Kirim
s.sendall(n)
s.close()