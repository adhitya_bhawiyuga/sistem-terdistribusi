import socket

# Server IP address
SERVER_IP = '127.0.0.1'
# Port number
PORT = 6666
# Inisialisasi object socket
s = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
# connect to server
s.connect((SERVER_IP, PORT))

user_input = raw_input("Masukkan pesan yang akan dikirim : ")
# Kirim
s.sendall(user_input)
# Baca kiriman balik dari Server
data, address = s.recvfrom(4096)
print 'The server answer ', data
s.close()