import socket
import pickle

class Mahasiswa(object) :
	nama = ""
	nim = ""

	def __init__(self,namaMhs, nimMhs) :
		self.nama = namaMhs
		self.nim = nimMhs

# Port number
PORT = 6666
# Inisialisasi socket TCP
s = socket.socket(socket.AF_INET, socket.SOCK_STREAM)

# Bind Port
s.bind(('', PORT))
# Listen
s.listen(10)

while True:
	# Accepting incoming connection
	conn, address = s.accept()
	# Baca pesan dari client
	data, address = conn.recvfrom(4096)
	# Unmarshalling
	mhs = pickle.loads(data)
	print "Nama mahasiswa : ", mhs.nama, " NIM ", mhs.nim
	conn.close()