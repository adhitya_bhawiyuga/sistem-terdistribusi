import paho.mqtt.client as mqtt
from thread import start_new_thread



# Callback fungsi ketika connect
def on_connect(mqttc, obj, flags, rc):
    print("Reconnect")
# Callback fungsi ketika menerima message
def on_message(mqttc, obj, msg):
    print("Received : "+msg.topic+" "+str(msg.qos)+" "+str(msg.payload))
# Callback fungsi ketika publish
def on_publish(mqttc, obj, mid):
    print("Published : "+str(mid))
# Callback fungsi ketika subscribe
def on_subscribe(mqttc, obj, mid, granted_qos):
    print("Subscribed: "+str(mid)+" "+str(granted_qos))
# Callback fungsi untuk logging
def on_log(mqttc, obj, level, string):
    print(string)

# Inisiasi client
user_id = raw_input("User ID : ")
mqttc = mqtt.Client(user_id, clean_session=True)
# Registrasi fungsi callback yang dipanggil setiap ada event tertentu
mqttc.on_message = on_message
mqttc.on_connect = on_connect
# Connect ke broker
mqttc.connect("127.0.0.1", 1883, 60)
user_sub = raw_input("Topik subscribe : ")
# Subscribe ke topik tertentu
mqttc.subscribe(user_sub, 0)
# Start thread loop
mqttc.loop_start()

while True :
	user_pub = raw_input("Topik publish : ")
	user_input = raw_input("Masukkan kata-katanya : ")
	mqttc.publish(user_pub, user_input)
