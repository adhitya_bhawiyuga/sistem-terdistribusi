import paho.mqtt.client as mqtt

# Inisiasi client dengan ID "sub1"
mqttc = mqtt.Client("pub1", clean_session=True)
# Koneksi ke broker
mqttc.connect("127.0.0.1", 1883)
# Publish ke topik "ptiik/sister/a"
mqttc.publish("ptiik/sister/a", payload="Hello World!!", qos=0)
mqttc.publish("ptiik/sister/b", payload="Ini dari B", qos=0)