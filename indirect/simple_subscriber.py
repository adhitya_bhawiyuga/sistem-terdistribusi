import paho.mqtt.client as mqtt

# Callback fungsi ketika connect
def on_connect(mqttc, obj, flags, rc):
    print("Reconnect")
# Callback fungsi ketika menerima message
def on_message(mqttc, obj, msg):
    print("Received : "+msg.topic+" "+str(msg.qos)+" "+str(msg.payload))
# Callback fungsi ketika publish
def on_publish(mqttc, obj, mid):
    print("Published : "+str(mid))
# Callback fungsi ketika subscribe
def on_subscribe(mqttc, obj, mid, granted_qos):
    print("Subscribed: "+str(mid)+" "+str(granted_qos))
# Callback fungsi untuk logging
def on_log(mqttc, obj, level, string):
    print(string)

# Inisiasi client dengan ID "sub1"
mqttc = mqtt.Client("sub1")
# Registrasi fungsi callback yang dipanggil setiap ada event tertentu
mqttc.on_message = on_message
mqttc.on_connect = on_connect
mqttc.on_publish = on_publish
mqttc.on_subscribe = on_subscribe
# Koneksi ke broker
mqttc.connect("127.0.0.1", 1885)
# Subscribe ke topik "ptiik/sister/a" 
mqttc.subscribe("ptiik/#", qos=1)
# Start looping forever
mqttc.loop_forever()
