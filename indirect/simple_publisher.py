import paho.mqtt.client as mqtt

# Inisiasi client dengan ID "sub1"
mqttc = mqtt.Client("pub1", clean_session=True)
# Koneksi ke broker
mqttc.connect("127.0.0.1", 1885)
# Publish ke topik "ptiik/sister/a"
mqttc.publish("ptiik/sister/a", payload="Ini dari Sister A", qos=1)
mqttc.publish("ptiik/sister/b", payload="Ini dari Sister B", qos=1)
mqttc.publish("ptiik/jarkom/b", payload="Ini dari Jarkom B", qos=1)